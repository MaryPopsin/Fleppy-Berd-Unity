﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnPoolModif : MonoBehaviour {
    #region Inner classes
    
    [System.Serializable]
    public class SpawnConfig
    {
        /// scroll speed of the game
        [Range(0.5f, 2.5f)] //
        public float ScrollSpeed;

        /// Spawn position x of the first column
        public float StartX;

        ///min spawn pos X of the next column
        public float SpawnMin;
       
        /// max spawn pos X of the next column
        public float SpawnMax;

        /// min spawn pos Y of the next column 
        public float HMin;
       
        /// max spawn pos Y of the next column
        public float HMax;

        ///min spacing between the top and the bottom
        public float SpacingMin;
        
        ///max spacing between the top and the bottom
        public float SpacingMax;
  
        /// chance of a column to have a top and a bottom part
        [Range(0, 1)]
        public float PrctDouble = 0.5f;
    }
    #endregion

    #region Variable
    public int ColumnPoolSize = 5;
    public GameObject columnPrefab;
    //public float spawnRate = 4f;
    //public float columnMin = -1f;
    //public float columnMax = 3.5f;


    private GameObject[] columns;
    //private Vector2 objectPoolPosition = new Vector2(-15f, -25f);
    //private float timeSinceLastSpawned;
    //private float spawnXPosition = 10f;
    private int currentColumn = 0;

    public SpawnConfig Config; // la faire apparaitre dans inspector
    #endregion



    // Use this for initialization
    void Start()
    {
        // créer un tableau de GameObject de taille ColumnPoolSize
        columns = new GameObject[ColumnPoolSize];
        for (int i = 0; i < ColumnPoolSize; i++)
        {
            // crée un objet colonne
            //Stocke une réference vers l'objet dans le tableau
            columns[i] = Instantiate(columnPrefab);
            // désactiver l'objet
            columns[i].SetActive(false);

        }
        SpawnColumn(Config, 0);
    }
    void SpawnColumn(SpawnConfig config, float lastColumnPositionX)
    {          
        //case du tableau qui contient la colonne à positionner et à activer
        var nextColumn = currentColumn + 1 >= ColumnPoolSize ? 0 : currentColumn + 1;

        //récupère la colonne
        var column = columns[nextColumn];

        //on l'active
        column.SetActive(true);

        Vector3 position = new Vector3(lastColumnPositionX, 0, 0);
        //positionnement en X (spawn min / spawn max)
        position.x += Random.Range(config.SpawnMin, config.SpawnMax);
        //positionnement en Y (HMin, HMax)
        position.y += Random.Range(config.HMin, config.HMax);

        column.transform.position = position;

        //est une colonne avec deux partie ?
        bool IsDouble = Random.Range(0f, 1f) < config.PrctDouble;

        //réactiver tout les enfants
        foreach (Transform child in column.transform)
        {
            child.gameObject.SetActive(true);
        }


        //si ce n'est pas une colonne à deux parties
        if (!IsDouble) {
            //l'enfant a faire disparaître
            var childCount = column.transform.childCount;
            var hidePart = column.transform.GetChild(Random.Range(0, childCount));
            //Cacher l'enfant
            hidePart.gameObject.SetActive(false);
        }

        //Espacement entre les colonnes
        var spacing = Random.Range(config.SpacingMin, config.SpacingMax);
        column.transform.GetChild(0).localPosition = Vector3.down * spacing * 0.5f;
        column.transform.GetChild(1).localPosition = Vector3.up * spacing * 0.5f;

        //mise a jour de la colonne actuelle
        currentColumn = nextColumn;
    }
    
    private void Update()
    {
        if (columns[currentColumn].transform.position.x <= 8)
        {
            SpawnColumn(Config, columns[currentColumn].transform.position.x);
        }
    }
    
    
}
