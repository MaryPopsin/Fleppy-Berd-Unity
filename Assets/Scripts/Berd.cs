﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Berd : MonoBehaviour {

    public float upForce = 200f;
    private bool isDead = false;
    private Rigidbody2D rb2d;
    private Animator anim;
 

    
    // Use this for initialization
    void Start () {

        rb2d = GetComponent<Rigidbody2D> ();
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        if (isDead == false)
        {
            if (Input.GetMouseButtonDown(0))
            {
                rb2d.velocity = Vector2.zero; // body always rising or falling, blocked on (0,0)
                rb2d.AddForce(new Vector2(0, upForce));
                anim.SetTrigger("Flep");
            }
        }

        		
	}

    void OnCollisionEnter2D()
    {
        rb2d.velocity = Vector2.zero;
        isDead = true; //peu importe ce qu'on fait berd reste sur le ground
        anim.SetTrigger("Ded");
        GameControl.instance.BirdDed();

    }

   
    
}
