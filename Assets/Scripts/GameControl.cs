﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {

    public static GameControl instance;
    public GameObject gameOverText;
    public Text scoreText;
    public bool gameOver = false;
    public float scrollSpeed = -1.5f;

    public int HighScore { get; protected set; } //pour protéger notre set

    private int score = 0;

    //HIGHSCORE
    private int Score //Propriétés : a variable for which you can define your onw SET and GET methods, marche comme une variable mais nous definissons ce qu'elle fait
    {
        get
        {
            return score;
        }

        set
        {   // vérifies si nouvelle valuer différente de l'ancienne
            score = value;
            if (score > HighScore)
            {
                HighScore = score;
                PlayerPrefs.SetInt("HighScore", HighScore); //playerprefs >> pas sécurisé, facilement hackable/ Sauvegarde le highscore
            } 
                       
        }

    }

    private void Awake() // vérifie s'il y a dejà un gamecontroller déjà instancier, s'il y en a un il le détruit.
    {
        if (instance == null)
        {
            instance = this;
        }  

        else if (instance != this)
        {
            Destroy(gameObject);
        }
        // On récupère le HighScore du cache
        HighScore = PlayerPrefs.GetInt("HighScore", 0);
    }
    // Update is called once per frame
	void Update () {
		if (gameOver == true && Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }// to restart the scene//game

    }

    public void BerdScored()
    {
        if (gameOver)
        {
            return;
        }
        Score++;
        scoreText.text = "Score:" + Score.ToString (); //montrer le score incrementé de 1
    }
    public void BirdDed()
    {
        gameOverText.SetActive(true);
        gameOver = true;
    }

   
  
   

}
